import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://freesvg.org/img/bikeicon.png'),
    NetworkImage(
        'https://i.pinimg.com/564x/78/fd/7f/78fd7f7f25b81cf11f6913b429b8d6fc.jpg'),
    NetworkImage(
        'https://upload.wikimedia.org/wikipedia/commons/5/55/Black_bus_icon.jpg'),
    NetworkImage(
        'https://upload.wikimedia.org/wikipedia/commons/5/5a/Car_icon_alone.png'),
    NetworkImage(
        'https://cdn.pixabay.com/photo/2012/04/18/23/03/railway-38185_960_720.png'),
    NetworkImage('https://freesvg.org/img/running-icon.png'),
    NetworkImage(
        'https://previews.123rf.com/images/get4net/get4net1711/get4net171100271/89001729-subway-icon.jpg?fj=1'),
    NetworkImage(
        'https://as2.ftcdn.net/v2/jpg/01/01/41/99/1000_F_101419995_v8EYd2Z5PSLRSvY6ufOcNdcoEyrIyH8k.jpg'),
    NetworkImage(
        'https://previews.123rf.com/images/martialred/martialred1812/martialred181200030/113393191-una-persona-caminando-o-caminando-signo-icono-de-vector-plano-para-aplicaciones-y-sitios-web.jpg?fj=1'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
